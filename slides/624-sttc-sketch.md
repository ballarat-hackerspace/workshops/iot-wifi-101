---
group: "subscribing_to_the_cloud"
---
## The Sketch

Now for our sketch. Here is a worked example you can enter straight into the IDE:

```arduino
// Project 2 - Subscribing to the cloud
#include "BHStreams.h"

#define LED 27

const char* ssid = "INSERT SSID";
const char* password = "INSERT PASSWORD";

BHStreams bhs;

void flash() {
  digitalWrite(LED, HIGH);
  delay(250);
  digitalWrite(LED, LOW);
  delay(250);
}

void onData(BHStreams_Data *data) {
  flash();

  Serial.printf("got message: %s\n", data->toChar());
}

void setup() {
  Serial.begin(115200);

  pinMode(LED, OUTPUT);

  bhs.begin(ssid, password);
  bhs.connect();
  bhs.onFeed("iot-control", onData);

  flash();
  flash();

  Serial.println("setup() complete.");
}

void loop() {
  bhs.process(); // manage WiFi connection, and any MQTT or OTA messages
}
```
