---
group: "subscribing_to_the_cloud"
---
## The Hardware

Here's what you'll need to create this project:

  * One LED
  * One breadboard
  * Various connecting wires
  * HUZZAH32 and USB cable

We will connect the LED to digital pins 27.
