---
group: "subscribing_to_the_cloud"
---
## Challenges

Try and complete the following:

  1. Subscribe to other feeds.
  2. Uniquely flash the LED for different feeds.
