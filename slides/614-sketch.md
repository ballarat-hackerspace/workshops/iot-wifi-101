---
group: "blinking_led_wave"
---
## The Sketch

Now for our sketch. Here is a worked example you can enter straight into the IDE:

```arduino
// Project 1 - Creating a Blinking LED Wave
void setup() {
  Serial.begin(115200);

  pinMode(27, OUTPUT); // LED 1 control pin is set up as an output
  pinMode(33, OUTPUT); // same for LED 2 to LED 5
  pinMode(15, OUTPUT);
  pinMode(32, OUTPUT);
  pinMode(14, OUTPUT);

  Serial.println("setup() complete.");
}

void loop() {
  digitalWrite(27, HIGH); // turn LED 1 on
  delay(500);             // wait half a second
  digitalWrite(27, LOW);  // turn LED 1 off
  digitalWrite(33, HIGH); // and repeat for LED 2 to 5
  delay(500);
  digitalWrite(33, LOW);
  digitalWrite(15, HIGH);
  delay(500);
  digitalWrite(15, LOW);
  digitalWrite(32, HIGH);
  delay(500);
  digitalWrite(32, LOW);
  digitalWrite(14, HIGH);
  delay(500);
  digitalWrite(14, LOW);
  digitalWrite(32, HIGH);
  delay(500);
  digitalWrite(32, LOW);
  digitalWrite(15, HIGH);
  delay(500);
  digitalWrite(15, LOW);
  digitalWrite(33, HIGH);
  delay(500);
  digitalWrite(33, LOW);
  // the loop() will now loop around and start from the top again
}
```
