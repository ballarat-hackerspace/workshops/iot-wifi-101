---
group: iot-protocols
---
## Cellular

 * Standard: GSM/GPRS/EDGE (2G), UMTS/HSPA (3G), LTE (4G)
 * Frequencies: 900/1800/1900/2100MHz
 * Range: 35km max for GSM; 200km max for HSPA
 * Data Rates (typical download): 35-170kps (GPRS), 120-384kbps (EDGE), 384Kbps-2Mbps (UMTS), 600kbps-10Mbps (HSPA), 3-10Mbps (LTE)
