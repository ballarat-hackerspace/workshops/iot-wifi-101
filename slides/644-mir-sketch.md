---
group: "make_it_rain"
---
## The Sketch

Now for our sketch. Here is a worked example you can enter straight into the IDE:

```arduino
// Project 4 - Making it rain
#include "Adafruit_CCS811.h"
#include "BHStreams.h"

#define LED 27
#define TEMPERATURE 26 // A0=26, A1=25, A2=34
#define BUTTON 33

const char* ssid = "INSERT SSID";
const char* password = "INSERT PASSWORD";


Adafruit_CCS811 ccs;
BHStreams bhs;
uint32_t lastButton = 0;
uint32_t lastGas = 0;
uint32_t lastTemp = 0;

void flash() {
  digitalWrite(LED, HIGH);
  delay(250);
  digitalWrite(LED, LOW);
  delay(250);
}

void onData(BHStreams_Data *data) {
  flash();

  Serial.printf("got message: %s\n", data->toChar());
}

void setup() {
  Serial.begin(115200);

  pinMode(LED, OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);
  pinMode(TEMPERATURE, INPUT);

  if (!ccs.begin()){
    Serial.println("Failed to start sensor! Please check your wiring.");
    while(1);
  }

  bhs.begin(ssid, password);
  bhs.connect();
  bhs.onFeed("iot-control", onData);

  flash();
  flash();

  Serial.println("setup() complete.");
}

void loop() {
  bhs.process(); // manage WiFi connection, and any MQTT or OTA messages

  uint32_t now = millis();

  if (!digitalRead(BUTTON) && (now - lastButton > 1000)) {
    bhs.write("iot-announce", "My Feather");
    Serial.println("data sent.");
    lastButton = now;
  }

  // read gas sensor when data is available
  if ((now - lastGas) > 30000 && ccs.available() && !ccs.readData()) {  // readData returns true on error
    int eCO2 = ccs.geteCO2();                // collect eCO2
    int TVOC = ccs.getTVOC();                // collect TVOC
    float temp = ccs.calculateTemperature(); // approximate ambient temperature

    bhs.write("iot/eCO2/value", eCO2);
    bhs.write("iot/TVOC/value", TVOC);

    lastGas = now;
  }

  // converting that reading to voltage, for 3.3v arduino use 3.3
  if ((now - lastTemp) > 30000) {
    float voltage = analogRead(TEMPERATURE) * 3.3 / 4096.0;
    float temperatureC = (voltage - 0.5) * 100 ;  //converting from 10 mv per degree wit 500 mV offset to degrees ((voltage - 500mV) times 100)
    Serial.printf("%f degrees C\n", temperatureC);

    bhs.write("iot/temperature/value", temperatureC);

    lastTemp = now;
  }
}
```
