---
group: "tft_joystick"
---
## The Sketch

Now for our sketch. Here is a worked example you can enter straight into the IDE:

```arduino
#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>
#include <Adafruit_miniTFTWing.h>
#include <ESP32Servo.h>
#include "BHStreams.h"

#define ANGLE_STEP_SIZE 1

#define TFT_RST  -1    // we use the seesaw for resetting to save a pin
#define TFT_CS   14
#define TFT_DC   32
#define SERVO    15

const char* ssid = "INSERT SSID";
const char* password = "INSERT PASSWORD";

Adafruit_miniTFTWing ss;
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);
BHStreams bhs;
Servo servo;

int angle = 0;                   // servo position in degrees
int angleDir = ANGLE_STEP_SIZE;  // servo angle direction and magnitude
uint32_t lastUpdate = 0;

void onData(BHStreams_Data *data) {
  Serial.printf("got message: %s\n", data->toChar());
}

void onServoData(BHStreams_Data *data) {
  int newAngle = constrain(data->toUnsignedInt(), 0, 180);

  Serial.printf("got angle: %d\n", newAngle);

  if (angle - newAngle > 0) {
    angleDir = ANGLE_STEP_SIZE;
  } else {
    angleDir = -ANGLE_STEP_SIZE;
  }

  angle = newAngle;
  servo.write(angle);
}

void setup()   {
  Serial.begin(115200);

  pinMode(SERVO, OUTPUT);
  servo.attach(SERVO);

  if (!ss.begin()) {
    Serial.println("Seesaw couldn't be found!");
    while(1);
  }

  Serial.print("Seesaw started! Version: ");
  Serial.println(ss.getVersion(), HEX);

  bhs.begin(ssid, password);
  bhs.connect();
  bhs.onFeed("iot-control", onData);
  bhs.onFeed("iot-control/servo", onServoData);

  ss.tftReset();                         // reset the display
  ss.setBacklight(TFTWING_BACKLIGHT_ON); // turn off the backlight

  tft.initR(INITR_MINI160x80);           // initialize a ST7735S chip, mini display
  Serial.println("TFT initialized");

  tft.setRotation(1);

  tft.fillScreen(ST77XX_RED);
  delay(100);
  tft.fillScreen(ST77XX_GREEN);
  delay(100);
  tft.fillScreen(ST77XX_BLUE);
  delay(100);
  tft.fillScreen(ST77XX_BLACK);

  Serial.println("setup() complete.");
}

void loop() {
  bhs.process(); // manage WiFi connection, and any MQTT or OTA messages

  uint32_t now = millis();
  uint32_t buttons = ss.readButtons();
  uint16_t color = ST77XX_BLACK;

  if (!(buttons & TFTWING_BUTTON_LEFT)) {
    Serial.println("LEFT");
    color = ST77XX_WHITE;
  }
  tft.fillTriangle(150, 30, 150, 50, 160, 40, color);

  color = ST77XX_BLACK;
  if (!(buttons & TFTWING_BUTTON_RIGHT)) {
    Serial.println("RIGHT");
    color = ST77XX_WHITE;
  }
  tft.fillTriangle(120, 30, 120, 50, 110, 40, color);

  color = ST77XX_BLACK;
  if (!(buttons & TFTWING_BUTTON_DOWN)) {
    Serial.println("DOWN");
    color = ST77XX_WHITE;
  }
  tft.fillTriangle(125, 26, 145, 26, 135, 16, color);

  color = ST77XX_BLACK;
  if (!(buttons & TFTWING_BUTTON_UP)) {
    Serial.println("UP");
    color = ST77XX_WHITE;
  }
  tft.fillTriangle(125, 53, 145, 53, 135, 63, color);

  color = ST77XX_BLACK;
  if (!(buttons & TFTWING_BUTTON_A)) {
    Serial.println("A");
    color = ST7735_GREEN;
  }
  tft.fillCircle(30, 57, 10, color);

  color = ST77XX_BLACK;
  if (!(buttons & TFTWING_BUTTON_B)) {
    Serial.println("B");
    color = ST77XX_YELLOW;
  }
  tft.fillCircle(30, 18, 10, color);

  color = ST77XX_BLACK;
  if (!(buttons & TFTWING_BUTTON_SELECT)) {
    Serial.println("SELECT");
    color = ST77XX_WHITE;
  }
  tft.fillCircle(80, 40, 7, color);

  // every 0.5 seconds, continue to sweep back and forth
  if ((now - lastUpdate) > 500) {
    if ((angle + angleDir) < 0 || (angle + angleDir) > 180) {
      angleDir = -angleDir;
    }

    angle += angleDir;
    servo.write(angle);

    lastUpdate = now;
  }
}
```
