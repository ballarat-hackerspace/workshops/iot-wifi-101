---
group: "subscribing_to_the_cloud"
---
## The Algorithm

Here's our algorithm for the project:

  * Connect to a WiFi network.
  * Connect to the public Ballarat Hackerspace Streams (MQTT) service.
  * Subscribe to a data topic.
  * Turn on an LED when we receive data.
  * Wait half a second.
  * Turn off LED.
  * Repeat indefinitely.

