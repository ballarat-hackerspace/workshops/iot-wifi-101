---
group: "blinking_led_wave"
---
## The Sketch - Refactor 2

We can use an array to organise our pin assignments in programmatical way.

```arduino
// Project 1 - Creating a Blinking LED Wave
#define LED1 27
#define LED2 33
#define LED3 15
#define LED4 32
#define LED5 14

int leds[5] = {LED1, LED2, LED3, LED4, LED5};

void setup() {
  Serial.begin(115200);

  pinMode(LED1, OUTPUT); // LED 1 control pin is set up as an output
  pinMode(LED2, OUTPUT); // same for LED 2 to LED 5
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);
  pinMode(LED5, OUTPUT);

  Serial.println("setup() complete.");
}

void loop() {
  for (int f=0; f<5; f++) {
    digitalWrite(leds[f], HIGH); // Turn LED on
    delay(500);                  // wait half a second
    digitalWrite(leds[f], LOW);  // Turn LED off
  }

  for (int r=4; r>=1; r--) {
    digitalWrite(leds[r], HIGH); // Turn LED on
    delay(500);                  // wait half a second
    digitalWrite(leds[r], LOW);  // Turn LED off
  }

 // the loop() will now loop around and start from the top again
}
```
