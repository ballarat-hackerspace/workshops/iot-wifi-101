---
group: iot-protocols
---
## WiFi

 * Standard: Based on 802.11n (most common usage in homes today)
 * Frequencies: 2.4GHz and 5GHz bands
 * Range: Approximately 50m
 * Data Rates: 600 Mbps maximum, but 150-200Mbps is more typical, depending on channel frequency used and number of antennas (latest 802.11-ac standard should offer 500Mbps to 1Gbps)
