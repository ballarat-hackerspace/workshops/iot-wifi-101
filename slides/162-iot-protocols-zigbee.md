---
group: iot-protocols
---
## ZigBee

* Standard: ZigBee 3.0 based on IEEE802.15.4
* Frequency: 2.4GHz
* Range: 10-100m
* Data Rates: 250kbps
