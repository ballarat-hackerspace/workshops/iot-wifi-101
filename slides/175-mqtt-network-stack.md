---
group: mqtt
---
## Network Stack

* Sits at layers 5-7 of the OSI network model

<div class="full-image">
  <img src="./img/mqtt-network-protocols.png" />
</div>
