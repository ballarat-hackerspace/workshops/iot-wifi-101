---
group: "hardware"
---
## Gas Sensor

* Measures wide range of Volatile Organic Compounds (VOCs)
* I2C Interface

<div class="full-image">
  <img src="./img/parts-gas-sensor.png" />
</div>
