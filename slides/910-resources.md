---
group: "wrap-up"
---
## Resources

The Ballarat Hackerspace

  * Open Mondays, Tuesdays and Saturdays.
  * See [website](ballarathackerspace.org.au) for details.

