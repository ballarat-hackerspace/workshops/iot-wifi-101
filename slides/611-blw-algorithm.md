---
group: "blinking_led_wave"
---
## The Algorithm</h2>

Here's our algorithm for the project:

  * Turn on LED 1.
  * Wait half a second.
  * Turn off LED 1.
  * Turn on LED 2.
  * Wait half a second.
  * Turn off LED 2.
  * Continue until LED 5 is turned on, at which point the process reverses from LED 5 to 1.
  * Repeat indefinitely.
