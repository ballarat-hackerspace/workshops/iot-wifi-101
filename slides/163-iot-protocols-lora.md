---
group: iot-protocols
---
## Lora

 * Standard: LoRa
 * Frequency: Various
 * Range: 2-5km (urban environment), 15km (suburban environment)
 * Data Rates: 0.3-50 kbps
