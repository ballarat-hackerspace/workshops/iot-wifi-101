---
group: "arduino"
---
## Adafruit Feather HUZZAH32

<div class="feature-image">
  <img src="https://core-electronics.com.au/media/catalog/product/cache/1/image/650x650/fe1bcd18654db18f328c2faaaf3c690a/3/4/3405-00_1.jpg" />
</div>

USB-to-Serial converter, Automatic bootloader reset, Lithium Ion/Polymer charger, and a lot of the GPIOs brought out so you can use it with any of the Feather Wings.

The dual core ESP32 has both WiFi and Bluetooth Classic/LE support which means it's perfect for just about any wireless or Internet-connected project.
