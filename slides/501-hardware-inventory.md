---
group: "hardware"
---
## Inventory
Here's what you'll need in order to complete this workshop:

  * Adafruit Feather HUZZAH32
  * Breadboard
  * M/M Jumper Wires
  * 5x LEDs
  * TMP36
  * 9g Servo
  * CO2 Sensor
  * 3x NeoPixels
  * Rotaty Encoder with Push Button

<div class="feature-image">
  <img src="./img/all_bb.png" />
</div>
