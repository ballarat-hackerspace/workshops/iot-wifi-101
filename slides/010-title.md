---
layout: "title"
---
# The Internet of Things &ndash; 101
### An Arduino/ESP32 Workshop

<div class="sponsors">
  <img src="https://startupballarat.com.au/img/Startup-Ballarat-Logo.png" />
  <img src="https://ballarathackerspace.org.au/images/bhack-logo-title-black.svg" />
</div>
