---
group: "gauges_and_visuals"
---
## Challenges
Try and complete the following:

  1. Use servo as a guage for external data
  2. Use the Neopixels to visually represent the temperature in Paris
