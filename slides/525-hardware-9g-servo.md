---
group: "hardware"
---
## 9g Servo

* Precision controlled angle between 0 and 180 degrees
* PWM input

<div class="full-image">
  <img src="./img/parts-9g-servo.png" />
</div>
