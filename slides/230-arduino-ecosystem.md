---
group: "arduino"
---
## Arduino - Ecosystem

Arduino compatible devices come in a variety of form factors from the tiny to the large.

Many different microcontrollers are supported by the Arduino ecosystem and thus their capabilities also vary considerably.

<div class="feature-image">
  <img src="https://cdn.sparkfun.com/r/600-600/assets/7/1/9/0/c/5228fb7a757b7fb7568b456d.png" />
  <img src="https://core-electronics.com.au/media/catalog/product/cache/1/image/650x650/fe1bcd18654db18f328c2faaaf3c690a/3/4/3405-00_1.jpg" />
</div>
