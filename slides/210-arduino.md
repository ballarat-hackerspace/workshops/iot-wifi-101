---
group: "arduino"
---
## Arduino

Arduino is an open-source platform used for building electronics projects. Arduino consists of both a physical programmable circuit board and a piece of software, or IDE that runs on your computer, used to write and upload code to the physical board.

  * Single board microcontroller
  * 8-bit Atmel AVR or 32-bit Atmel ARM
  * UNO offers 6 analog inputs pins and 14 digital I/O pins
  * Intially released in 2005<

<div class="feature-image">
  <img src="https://cdn.sparkfun.com/assets/9/1/e/4/8/515b4656ce395f8a38000000.png" />
</div>
