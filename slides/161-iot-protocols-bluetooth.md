---
group: iot-protocols
---
## Bluetooth

* Standard: Bluetooth 4.2 core specification
* Frequency: 2.4GHz (ISM)
* Range: 50-150m (Smart/BLE)
* Data Rates: 1Mbps (Smart/BLE)
