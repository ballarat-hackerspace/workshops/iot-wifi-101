---
group: "hardware"
---
## Rotary Encoder


* Pulsed output on rotation
* A and B phase for direction detection
* Momentary push button

<div class="full-image">
  <img src="./img/parts-rotary-encoder.png" />
</div>
