---
group: "arduino"
---
## Arduino - Simplicity</h2>

  * Easy - Programmed based on common language (C/C++ compatible)
  * Cheap
  * Low Energy - ~0.1W
  * Durable - fanless, little to no heat
  * Open source, open hardware

<div class="feature-image">
  <img src="https://upload.wikimedia.org/wikipedia/commons/a/a1/Arduino_IDE_-_Blink.png" />
  <img src="http://www.krystalinkz.com/wp-content/uploads/2017/01/microusb.jpg" />
</div>

