---
group: "gauges_and_visuals"
---
## The Sketch

Now for our sketch. Here is a worked example you can enter straight into the IDE:

```arduino
// Project 5 - Gauges and Visuals
#include <Adafruit_NeoPixel.h>
#include <ESP32Servo.h>
#include "BHStreams.h"

#define ANGLE_STEP_SIZE 1

#define BUTTON 15
#define NEOPIXELS 33
#define SERVO 14

const char* ssid = "INSERT SSID";
const char* password = "INSERT PASSWORD";

Adafruit_NeoPixel strip = Adafruit_NeoPixel(2, NEOPIXELS, NEO_GRB + NEO_KHZ800);
BHStreams bhs;
Servo servo;

int angle = 0;                   // servo position in degrees
int angleDir = ANGLE_STEP_SIZE;  // servo angle direction and magnitude

uint32_t lastButton = 0;
uint32_t lastUpdate = 0;

void flash() {
  strip.setPixelColor(0, 255, 0, 0); // red
  strip.setPixelColor(1, 0, 255, 0); // green
  strip.show();
  delay(250);
  strip.setPixelColor(0, 0, 0, 0); // off
  strip.setPixelColor(1, 0, 0, 0); // off
  delay(250);
  strip.show();
}

void onData(BHStreams_Data *data) {
  flash();

  int newAngle = constrain(data->toUnsignedInt(), 0, 180);

  Serial.printf("got message: %d\n", newAngle);

  if (angle - newAngle > 0) {
    angleDir = ANGLE_STEP_SIZE;
  } else {
    angleDir = -ANGLE_STEP_SIZE;
  }

  angle = newAngle;
  servo.write(angle);
}

void setup() {
  Serial.begin(115200);

  pinMode(BUTTON, INPUT_PULLUP);
  pinMode(NEOPIXELS, OUTPUT);
  pinMode(SERVO, OUTPUT);

  strip.begin();
  flash();

  servo.attach(SERVO);

  bhs.begin(ssid, password);
  bhs.connect();
  bhs.onFeed("iot-control/servo", onData);

  flash();
  flash();

  Serial.println("setup() complete.");
}

void loop() {
  bhs.process(); // manage WiFi connection, and any MQTT or OTA messages

  uint32_t now = millis();

  if (!digitalRead(BUTTON) && (now - lastButton > 1000)) {
    bhs.write("iot-announce", "My Feather");
    Serial.println("data sent.");
    lastButton = now;
  }

  // every 0.5 seconds, continue to sweep and fade colours
  if ((now - lastUpdate) > 500) {
    if ((angle + angleDir) < 0 || (angle + angleDir) > 180) {
      angleDir = -angleDir;
    }

    angle += angleDir;
    servo.write(angle);

    lastUpdate = now;
  }
}
```
