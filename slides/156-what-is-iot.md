---
---
## What is the Internet of Things?

Network of physical devices, including but not limited to:

  * vehicles
  * humans,
  * home appliances, and other items embedded with electronics
  * cows,
  * sensors and actuators

IoT involves extending Internet connectivity beyond standard devices to any range of traditionally dumb or non-internet-enabled physical devices and everyday objects.

=====

The Internet of Things (IoT) is the network of physical devices, vehicles, home appliances, and other items embedded with electronics, software, sensors, actuators, and connectivity which enables these things to connect and exchange data, creating opportunities for more direct integration of the physical world into computer-based systems, resulting in efficiency improvements, economic benefits, and reduced human exertions.

IoT involves extending Internet connectivity beyond standard devices, such as desktops, laptops, smartphones and tablets, to any range of traditionally dumb or non-internet-enabled physical devices and everyday objects.

Embedded with technology, these devices can communicate and interact over the Internet, and they can be remotely monitored and controlled.
