---
group: "hardware"
---
## Neopixel

* RGB output
* Custom input protocol with strict timing requiremeints

<div class="full-image">
  <img src="./img/parts-neopixel.png" />
</div>
