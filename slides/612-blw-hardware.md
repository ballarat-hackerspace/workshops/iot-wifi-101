---
group: "blinking_led_wave"
---
## The Hardware
Here's what you'll need to create this project:

  * Five LEDs
  * One breadboard
  * Various connecting wires
  * HUZZAH32 and USB cable

We will connect the LEDs to digital pins 27, 33, 15, 32 and 14.
