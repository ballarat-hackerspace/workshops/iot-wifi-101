---
group: "hardware"
---
## LED

* Emits light when current passes through
* Polarised (current flows A to K)

<div class="full-image">
  <img src="./img/parts-led.png" />
</div>
