---
group: "blinking_led_wave"
---
## Challenges

Try and complete the following:

  1. Increase the speed of scanning.
  2. Start the scan from the centre LED, then out to the edge LEDs and back to the center LED.
