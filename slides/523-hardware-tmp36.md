---
group: "hardware"
---
## TMP36 - Temperature Sensor

* -40 to 150C
* Output voltage linearly proportional to temperature

<div class="full-image">
  <img src="./img/parts-tmp36.png" />
</div>
