---
group: "publishing_to_the_cloud"
---
## The Algorithm

Here's our algorithm for the project:

  * Connect to a WiFi network.
  * Connect to the public Ballarat Hackerspace Streams (MQTT) service.
  * Subscribe to a data topic.
  * Turn on an LED when we receive data, wait half a second and turn off.
  * Check if button is pushed and publish a unique ID to a data topic.
  * Repeat indefinitely.
