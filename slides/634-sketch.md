---
group: "publishing_to_the_cloud"
---
## The Sketch

Now for our sketch. Here is a worked example you can enter straight into the IDE:

```arduino
// Project 3 - Publishing to the cloud
#include "BHStreams.h"

#define LED 27
#define BUTTON 33

const char* ssid = "INSERT SSID";
const char* password = "INSERT PASSWORD";

BHStreams bhs;
uint32_t lastButton = 0;

void flash() {
  digitalWrite(LED, HIGH);
  delay(250);
  digitalWrite(LED, LOW);
  delay(250);
}

void onData(BHStreams_Data *data) {
  flash();

  Serial.printf("got message: %s\n", data->toChar());
}

void setup() {
  Serial.begin(115200);

  pinMode(LED, OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);

  bhs.begin(ssid, password);
  bhs.connect();
  bhs.onFeed("iot-control", onData);

  flash();
  flash();

  Serial.println("setup() complete.");
}

void loop() {
  bhs.process(); // manage WiFi connection, and any MQTT or OTA messages

  uint32_t now = millis();

  if (!digitalRead(BUTTON) && (now - lastButton > 1000)) {
    bhs.write("iot-announce", "My Feather");
    Serial.println("data sent.");
    lastButton = now;
  }
}
```
