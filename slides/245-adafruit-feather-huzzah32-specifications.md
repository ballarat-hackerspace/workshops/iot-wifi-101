---
group: "arduino"
---
## ESP32 Specifications

  * 240 MHz dual core microcontroller with 520KB SRAM
  * Integrated 802.11b/g/n WiFi, and dual mode Bluetooth (classic and BLE)
  * 4 MByte flash include in the WROOM32 module
  * On-board PCB antenna
  * Ultra-low noise analog amplifier
  * Hall sensor
  * 10x capacitive touch interface
  * 3 x UARTs
  * 3 x SPI (only one is configured by default in the Feather Arduino IDE support)
  * 2 x I2C (only one is configured by default in the Feather Arduino IDE support)
  * 12 x ADC input channels
  * 2 x I2S Audio
  * 2 x DAC
  * PWM/timer input/output available on every GPIO pin
  * and much, much more ...
