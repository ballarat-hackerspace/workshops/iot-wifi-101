---
group: "publishing_to_the_cloud"
---
## Challenges

Try and complete the following:

  1. Publish to your own channel.
  2. Publish unique data based on the duration/frequency of button presses.
