---
group: iot-protocols
---
## Protocols

<div class="full-image">
  <img src="./img/logo-protocols.png" />
</div>

Depending on the application, factors such as range, data requirements, security and power demands and battery life will dictate the choice of one or some form of combination of technologies.
