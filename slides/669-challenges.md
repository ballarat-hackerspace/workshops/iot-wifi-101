---
group: "tft_joystick"
---
## Challenges
Try and complete the following:

  1. Display the last topic value on the LCD
  2. Use the Servo to visually represent when the Hackerspace door is open
