---
group: "publishing_to_the_cloud"
---
## The Hardware

Here's what you'll need to create this project:

  * One LED
  * One push button
  * One breadboard
  * Various connecting wires
  * HUZZAH32 and USB cable
