<div class="slides">
  <section>
    <section>
      <h2>Wiring (C) Language / Arduino</h2>
      <p>As you learned in Module 01, IDE stands for Integrated Development Environment. Pretty fancy sounding, and should make you feel smart any time you use it. The IDE is a text editor-like program that allows you to write Arduino code.</p>
      <p>When you open the Arduino program, you are opening the IDE. It is intentionally streamlined to keep things as simple and straightforward as possible. When you save a file in Arduino, the file is called a sketch – a sketch is where you save the computer code you have written.</p>
      <p>The coding language that Arduino uses is very much like C++ (“see plus plus”), which is a common language in the world of computing. The code you learn to write for Arduino will be very similar to the code you write in any other computer language – all the basic concepts remain the same – it is just a matter of learning a new dialect should you pursue other programming languages.</p>
    </section>

    <section>
      <h2>The Semicolon ;</h2>
      <p>A semicolon needs to follow every statement written in the Arduino programming language.</p>

      <pre><code class="arduino">int LEDpin = 9;</code></pre>

      <p>In this statement, I am assigning a value to an integer variable (we will cover this later), notice the semicolon at the end. This tells the compiler that you have finished a chunk of code and are moving on to the next piece. A semicolon is to Arduino code, as a period is to a sentence. It signifies a complete statement.</p>
    </section>
    <section>
      <h2>The Double Backslash &ndash; //</h2>

      <p>Comments are what you use to annotate code. Good code is commented well.</p>

      <pre><code class="arduino">// When you type a double backslash all the text that follows on the same line will be grayed out</code></pre>

      <p>Comments are meant to inform you and anyone else who might stumble across your code, what the heck you were thinking when you wrote it.</p>

      <pre><code class="arduino">// This is the pin on the Arduino that the LED is plugged into
int LEDpin = 9;</code></pre>

      <p>Now, in 3 months when I review this program, I know where to stick my LED.</p>

    </section>

    <section>
      <h2>The Double Backslash &ndash; // & /* ... */</h2>

      <p>Comments will be ignored by the compiler – so you can write whatever you like in them. If you have a lot you need to explain, you can use a multi-line comments.</p>

      <pre><code class="arduino">/* The multi-line comment opens with a single backslash followed by an asterisk.
Everything that follows is grayed out and will be ignored by the compiler,
until you close the comment using first an asterisk and then a backslash like so */
      </code></pre>

      <p>Comments are like the footnotes of code, except far more prevalent and not at the bottom of the page.</p>

    </section>

    <section>
      <h2>Curly Braces &ndash; { }</h2>

      <p>Curly braces are used to enclose further instructions carried out by a function (we discuss functions next).</p>
      <p>There is always an opening curly bracket and a closing curly bracket. If you forget to close a curly bracket, the compiler will not like it and throw an error code.</p>

      <pre><code class="arduino">void loop() { // this curly brace opens
              // way cool program here
}             // this curly brace closes
      </code></pre>

      <p>Remember &ndash; no curly brace may go unclosed!</p>
    </section>

    <section>
      <h2>Functions</h2>

      <p>Functions are pieces of code that are used so often that they are encapsulated in certain keywords so that you can use them more easily. Consider the procedure for washing a dog.</p>

      <ol>
        <li>Get a bucket</li>
        <li>Fill it with water</li>
        <li>Add soap</li>
        <li>Find dog</li>
        <li>Lather dog</li>
        <li>Wash dog</li>
        <li>Rinse dog</li>
        <li>Dry dog</li>
        <li>Put away bucket</li>
      </ol>

      <p>This set of simple instructions could be encapsulated in a function called <code>WashDog()</code>. Every time we want to carry out all those instructions as prescribed we can just use <code>WashDog()</code>.</p>
    </section>

    <section>
      <h2>Functions</h2>
      <p>In Arduino, there are certain functions used so often that they are recognised by the IDE. A recognised function will appear orange. The function pinMode(), for example, is a common function used to designate the mode of an Arduino pin.</p>

      <pre><code class="arduino">pinMode(13, OUTPUT); // sets the mode of an Arduino pin</code></pre>

      <p>Many functions require arguments to work as they convey information the function uses when it runs.</p>
      <p>For our <code>WashDog()</code> function, the arguments might be dog name and soap type, or temperature and size of the bucket.</p>

      <p>The argument 13 refers to pin 13, and OUTPUT is the mode in which you want the pin to operate. When you enter these arguments the terminology is called passing. You pass the necessary information to the functions. Not all functions require arguments, but opening and closing parentheses will stay regardless though empty.</p>
    </section>

    <section>
      <h2>Functions</h2>
      <pre><code class="arduino">millis(); // retrieves the length of time in milliseconds that the Arduino has been running</code></pre>

      <p>Notice that the word OUTPUT is blue. There are certain keywords in Arduino that are used frequently and the color blue helps identify them. The IDE turns them blue automatically.</p>
      <p>Now we won’t get into it here, but you can easily make your own functions in Arduino, and you can even get the IDE to color them for you.</p>
      <p>We will, however, talk about the two functions used in nearly EVERY Arduino program.</p>
    </section>
    <section>
      <h2>Function &ndash; setup()</h2>

      <p>The core function, setup(), as the name implies, is used to set up the Arduino board.</p>
      <p>The Arduino executes all the code that is contained between the curly braces of setup() only once. Typical things that happen in setup() are setting the modes of pins.</p>

      <pre><code class="arduino">void setup() {
  // the code between the curly braces is only run once for setup()
}</code></pre>

      <p>You might be wondering what void means before the function setup(). Void means that the function does not return information.</p>

      <p>Some functions do return values – our DogWash function might return the number of buckets it required to clean the dog. The function analogRead() returns an integer value between 0-1023. If this seems a bit odd now, don’t worry as we will cover every common Arduino function in depth as we continue the course.</p>

      <p>Let us review a couple things you should know about setup() ...</p>
      <ul>
        <li>setup() only runs once.</li>
        <li>setup() needs to be the first function in your Arduino sketch.</li>
        <li>setup() must have opening and closing curly braces.</li>
      </ul>
    </section>

    <section>
      <h2>Function &ndash; loop()</h2>
      <p>You have to love the Arduino developers because the function names are so telling. As the name implies, all the code between the curly braces in loop() is repeated over and over again – in a loop. The loop() function is where the body of your program will reside.</p>

      <p>As with setup(), the function loop() does not return any values, therefore the word void precedes it.</p>

      <pre><code class="arduino">void loop() {
//whatever code you put here is executed over and over
}</code></pre>

      <p>Does it seem odd to you that the code runs in one big loop? This apparent lack of variation is an illusion. Most of your code will have specific conditions laying in wait which will trigger new actions.</p>

      <p>If you have a temperature sensor connected to your Arduino for example, then when the temperature gets to a predefined threshold you might have a fan kick on. The looping code is constantly checking the temperature waiting to trigger the fan. So even though the code loops over and over, not every piece of the code will be executed every iteration of the loop.</p>
    </section>

    <section>
      <h2>Data Types</h2>
    </section>

    <section>
      <h2>Exercises</h2>
      <p>This course is based around the example sketches provided with the Arduino IDE. Open up your Arduino IDE and go to File > Example > 01.Basics and open up three different sketches.</p>
      <p>Identify the following syntax and functions that you find in the sketches:</p>

      <ul>
        <li>; semi-colons</li>
        <li>// single line comments</li>
        <li>/* */ multi-line comments</li>
        <li>{ } open and closing curly braces</li>
        <li>( ) parenthesis</li>
        <li>void setup() – identify the opening and closing curly braces</li>
        <li>void loop() – identify the opening and closing curly braces</li>
        <li>some orange/blue keywords like OUTPUT or INPUT</li>
      </ul>
    </section>
  </section>
</div>
