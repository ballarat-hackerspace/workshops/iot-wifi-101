# IOT WiFi 101 Workshop

This workshop will walk you through a few common IoT scenarios from basic user input through to publishing to and responding from online sources.
You will learn:
  * How to navigate and use the Arduino IDE.
  * How to connect sensors and code.
  * Measure sensor values like temperature, air quality, etc and interact with these values via a web browser.
  * Make your first little Internet of Things project!
